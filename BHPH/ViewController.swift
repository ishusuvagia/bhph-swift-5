//
//  ViewController.swift
//  BHPH
//
//  Created by mojave on 17/07/19.
//  Copyright © 2019 mojave. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var window: UIWindow?
    
    @IBOutlet weak var loginBtn: UIButton!
    @IBOutlet weak var passwordTxt: UITextField!
    @IBOutlet weak var usernameTxt: UITextField!
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        usernameTxt.layer.cornerRadius = 10
        usernameTxt.layer.borderWidth = 1.0
        usernameTxt.layer.borderColor=UIColor.init(red: 3/255, green: 103/255, blue: 199/255, alpha: 1).cgColor
        usernameTxt.clipsToBounds = true
        let imageName = "username-icn"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        imageView.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: usernameTxt.frame.size.height))
        paddingView.addSubview(imageView)
        usernameTxt.leftView = paddingView
        usernameTxt.leftViewMode = .always
        
       
        passwordTxt.layer.cornerRadius = 10;
        passwordTxt.layer.borderWidth = 1.0;
        passwordTxt.layer.borderColor=UIColor.init(red: 3/255, green: 103/255, blue: 199/255, alpha: 1).cgColor;
        passwordTxt.clipsToBounds = true;
        let passImgName = "password-icn"
        let passimage = UIImage(named: passImgName)
        let passIV = UIImageView(image: passimage!)
        passIV.frame = CGRect(x: 10, y: 10, width: 20, height: 20)
        let paddingViewpass = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: passwordTxt.frame.size.height))
        paddingViewpass.addSubview(passIV)
        passwordTxt.leftView = paddingViewpass
        passwordTxt.leftViewMode = .always
    }

    @IBAction func loginBtn_clicked(_ sender: Any) {
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "dashboardVC") as! dashboardVC
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
        
    }
    
}

