
import UIKit
import Charts

class dashboardVC: UIViewController,UITableViewDataSource,UITableViewDelegate,ChartViewDelegate {

    @IBOutlet var chartView: PieChartView!

    
    let sideMenuList: [String] = [ "Dashboard", "Vehicle List","Vehicle Tracking", "History Playback", "Vehicle Newarby Me","Notification", "Report","Travel Summary","Setting", "Logout"]
    let sideMenuIconList: [String] = [ "icon-dashboard", "icon-vehicle","icon-tracking", "icon-history", "icon-nearby","icon-notifiaction", "icon-report","icon-travel","icon-setting", "icon-logout"]
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var btnSidemenu: UIButton!
    @IBOutlet weak var sideMenuView: UIView!
    @IBOutlet weak var sideMenuTable: UITableView!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        let myCustomViewController: DemoBaseViewController = DemoBaseViewController(nibName: "DemoBaseViewController", bundle: nil)
        myCustomViewController.setup(pieChartView: chartView)
        
        chartView.delegate = self
        self.updateChartData()
        chartView.animate(xAxisDuration: 1.0, easingOption: .easeInBounce)
        
        sideMenuTable.delegate = self
        sideMenuTable.dataSource = self

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        self.imageView.isUserInteractionEnabled = true
        self.imageView.addGestureRecognizer(tapGestureRecognizer)
    }
    
    func updateChartData() {
        let myCustomViewController: DemoBaseViewController = DemoBaseViewController(nibName: "DemoBaseViewController", bundle: nil)
        if myCustomViewController.shouldHideData {
            chartView.data = nil
            return
        }
        self.setDataCount(1, range:1)
        
//        let chart = PieChartView(frame: self.view.frame)
//        // 2. generate chart data entries
//        let track = ["Running", "NoData", "Stop", "Idle", "InActive"]
//        let money = [12, 4, 2, 6, 6]
//        /*
//         entries.append(PieChartDataEntry(value: 12*100, label: "Running"))
//         entries.append(PieChartDataEntry(value: 2*100, label: "NoData"))
//         entries.append(PieChartDataEntry(value: 4*100, label: "Stop"))
//         entries.append(PieChartDataEntry(value: 6*100, label: "Idle"))
//         entries.append(PieChartDataEntry(value: 6*100, label: "InActive"))
//         */
//        var entries = [PieChartDataEntry]()
//        for (index, value) in money.enumerated() {
//            let entry = PieChartDataEntry()
//            entry.y = Double(Int(value))
//            entry.label = track[index]
//            entries.append( entry)
//        }
//
//        // 3. chart setup
//        let set = PieChartDataSet( entries: entries, label: "")
//        // this is custom extension method. Download the code for more details.
////        var colors: [UIColor] = []
//
//        let c1 = UIColor(red: 0/255, green: 153/255, blue: 0/255, alpha:1)
//        let c2 = UIColor(red: 114/255, green: 114/255, blue: 114/255, alpha: 1)
//        let c3 = UIColor(red: 246/255, green: 65/255, blue: 64/255, alpha: 1)
//        let c4 = UIColor(red: 238/255, green: 195/255, blue: 12/255, alpha: 1)
//        let c5 = UIColor(red: 1/255, green: 153/255, blue: 204/255, alpha: 1)
//
//        set.colors = [c1,c2,c3,c4,c5]
//        let data = PieChartData(dataSet: set)
//        chart.data = data
//        chart.noDataText = "No data available"
//        // user interaction
//        chart.isUserInteractionEnabled = true
//
//
//        chart.centerText = "32 Total"
//        chart.holeRadiusPercent = 0.40
//        chart.transparentCircleColor = UIColor.clear
//        self.view.addSubview(chart)
    }
    
    func setDataCount(_ count: Int, range: UInt32) {
        
        var entries:[PieChartDataEntry] = Array()
        entries.append(PieChartDataEntry(value: 12, label: "Running"))
        entries.append(PieChartDataEntry(value: 2, label: "NoData"))
        entries.append(PieChartDataEntry(value: 4, label: "Stop"))
        entries.append(PieChartDataEntry(value: 6, label: "Idle"))
        entries.append(PieChartDataEntry(value: 6, label: "InActive"))
        
        
        let set = PieChartDataSet(entries: entries, label: "")
        set.drawIconsEnabled = false
        set.sliceSpace = 0
        
        let c1 = UIColor(red: 0/255, green: 153/255, blue: 0/255, alpha:1)
        let c2 = UIColor(red: 114/255, green: 114/255, blue: 114/255, alpha: 1)
        let c3 = UIColor(red: 246/255, green: 65/255, blue: 64/255, alpha: 1)
        let c4 = UIColor(red: 238/255, green: 195/255, blue: 12/255, alpha: 1)
        let c5 = UIColor(red: 1/255, green: 153/255, blue: 204/255, alpha: 1)
        
        set.colors = [c1,c2,c3,c4,c5]
        
        let data = PieChartData(dataSet: set)
        
        data.setValueFont(.systemFont(ofSize: 10, weight: .medium))
        data.setValueTextColor(.white)
        
        chartView.data = data
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
       
        self.sideMenuView.frame.origin.x -= self.view.bounds.width
        self.imageView.isHidden = true
        btnSidemenu.isSelected = false
    }
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.sideMenuView.frame.origin.x -= self.view.bounds.width
        }) { (isCompleted) in
            self.imageView.isHidden = true
        }
        btnSidemenu.isSelected = false
    }
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        print(entry.y)
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "vehicleListVC") as! vehicleListVC
        secondViewController.TotalReturn = Int(entry.y)
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
    }
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sideMenuList.count
    }

    // create a cell for each table view row
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //            let cell = self.owner_tableView.dequeueReusableCell(withIdentifier: "ownerCell", for: indexPath)
        //                as! ownerCell
        let cell = self.sideMenuTable.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let lbl_title:UILabel = cell.viewWithTag(1) as! UILabel
        lbl_title.text = sideMenuList[indexPath.row]
        
        let icon:UIImageView = cell.viewWithTag(2) as! UIImageView
        icon.image = UIImage(named: sideMenuIconList[indexPath.row])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 40.0;
    }
    
    // method to run when table view cell is tapped
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        print("You tapped cell number \(indexPath.row).")
        UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
            self.sideMenuView.frame.origin.x -= self.view.bounds.width
        }) { (isCompleted) in
            self.imageView.isHidden = true
        }
        btnSidemenu.isSelected = false
    }
    @IBAction func sideMenuBtnPressed(_ sender: Any)
    {
        if btnSidemenu.isSelected == true
        {
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                self.sideMenuView.frame.origin.x -= self.view.bounds.width
            }) { (isCompleted) in
                self.imageView.isHidden = true
            }
            btnSidemenu.isSelected = false
        }
        else
        {
            UIView.animate(withDuration: 0.6, delay: 0, options: .curveEaseInOut, animations: {
                self.sideMenuView.frame.origin.x = self.view.bounds.origin.x
                self.imageView.isHidden = false
            }) { (isCompleted) in   }
            btnSidemenu.isSelected = true
        }
        sideMenuTable.reloadData()
    }
}

