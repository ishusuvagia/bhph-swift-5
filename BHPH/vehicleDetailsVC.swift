import UIKit
import MapKit
import CoreLocation

class vehicleDetailsVC: UIViewController,MKMapViewDelegate,CLLocationManagerDelegate {

    @IBOutlet weak var scroll: UIScrollView!
    
    @IBOutlet weak var ScV: UIView!
    @IBOutlet weak var FullMapSc: UIView!
    
    @IBOutlet weak var StarterEnaBtn: UIButton!
    @IBOutlet weak var LocationNwBtn: UIButton!
    @IBOutlet weak var StarterDisBtn: UIButton!
    @IBOutlet weak var lastPositionBtn: UIButton!
    @IBOutlet weak var reportsBtn: UIButton!
    var mapStatus = ""
    @IBOutlet weak var MapView: MKMapView!
    @IBOutlet weak var FullMapView: MKMapView!
    
    

    var locationManager: CLLocationManager!

    
    
    //*********************************
    //Life Cycle
    //*********************************
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        scroll.contentSize = CGSize(width: ScV.frame.size.width, height: ScV.frame.size.height+10)
        FullMapSc.isHidden = true
        mapStatus = "off"
        if (CLLocationManager.locationServicesEnabled())
        {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.startUpdatingLocation()
        }
    
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.last{
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            MapView.setRegion(region, animated: true)
            FullMapView.setRegion(region, animated: true)
        }
    }
    @IBAction func backBtn_clicked(_ sender: Any)
    {
        if mapStatus == "on"
        {
            mapStatus = "off";
            FullMapSc.isHidden = true
        }
        else
        {
     self.navigationController?.popViewController(animated: true)
        }
        
        
    }


    @IBAction func starterEnableBtn_clicked(_ sender: Any) {
        StarterEnaBtn.setBackgroundImage(UIImage (named: "btn-green-sel"), for: .normal)
        LocationNwBtn.setBackgroundImage(nil, for: .normal)
        StarterDisBtn.setBackgroundImage(nil, for: .normal)
    }
    @IBAction func locationNowBtn_clicked(_ sender: Any) {
        StarterEnaBtn.setBackgroundImage(nil, for: .normal)
        LocationNwBtn.setBackgroundImage(UIImage (named: "btn-black-sel"), for: .normal)
        StarterDisBtn.setBackgroundImage(nil, for: .normal)
    }
    @IBAction func starterDisableBtn_clicked(_ sender: Any) {
        StarterEnaBtn.setBackgroundImage(nil, for: .normal)
        LocationNwBtn.setBackgroundImage(nil, for: .normal)
        StarterDisBtn.setBackgroundImage(UIImage (named: "btn-red-sel"), for: .normal)
    }
    @IBAction func lastPositionBtn_clicked(_ sender: Any) {
        lastPositionBtn.setBackgroundImage(UIImage (named: "btn-detailpage-sel"), for: .normal)
        reportsBtn.setBackgroundImage(UIImage (named: "btn-detailpage-des"), for: .normal)
        
        reportsBtn.setTitleColor(UIColor.black, for: .normal)
        lastPositionBtn.setTitleColor(UIColor.white, for: .normal)
    }
    @IBAction func reportsBtn_clicked(_ sender: Any) {
        reportsBtn.setBackgroundImage(UIImage (named: "btn-detailpage-sel"), for: .normal)
        lastPositionBtn.setBackgroundImage(UIImage (named: "btn-detailpage-des"), for: .normal)
        lastPositionBtn.setTitleColor(UIColor.black, for: .normal)
        reportsBtn.setTitleColor(UIColor.white, for: .normal)
    }
    @IBAction func fullScreenBtn_clicked(_ sender: Any) {
        mapStatus = "on"
        FullMapSc.isHidden = false
        
    }
   
}
