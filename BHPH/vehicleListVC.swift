import UIKit
class MyCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var stockLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var inventoryImg: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
}
class vehicleListVC: UIViewController,UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    let reuseIdentifier = "cell" // also enter this string as the cell identifier in the storyboard
    var items = ["1.jpeg", "2.jpeg", "3.jpeg", "4.jpeg", "5.jpeg", "6.jpeg", "7.jpeg", "8.jpeg","1.jpeg", "2.jpeg", "3.jpeg", "4.jpeg", "5.jpeg", "6.jpeg", "7.jpeg", "8.jpeg"]
    
    @IBOutlet weak var Search_friend_tf: UITextField!
    @IBOutlet weak var totalCountLbl: UILabel!
    @IBOutlet weak var listcollView: UICollectionView!
    
    var TotalReturn = Int()
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // flowlayout
        let screenWidth = UIScreen.main.bounds.width
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
        layout.itemSize = CGSize(width: screenWidth/2 - 10, height: 210)
//        layout.minimumInteritemSpacing = 5
//        layout.minimumLineSpacing = 5
        listcollView.collectionViewLayout = layout
        
        Search_friend_tf.layer.cornerRadius = 10
        Search_friend_tf.layer.borderWidth = 1.0
        Search_friend_tf.layer.borderColor = UIColor.init(red: 3/255, green: 103/255, blue: 199/255, alpha: 1).cgColor
        Search_friend_tf.clipsToBounds = true
        let imageName = "icon-search"
        let image = UIImage(named: imageName)
        let imageView = UIImageView(image: image!)
        imageView.frame = CGRect(x: Search_friend_tf.frame.size.width-30, y: 5, width: 20, height: 20)
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: 20, height: Search_friend_tf.frame.size.height))
        paddingView.addSubview(imageView)
        Search_friend_tf.leftView = paddingView
        Search_friend_tf.leftViewMode = .always
        totalCountLbl.text = NSString(format: "   %lu Result Found",TotalReturn) as String
    }
    
    
    //******************************************************************************
    // MARK: - UICollectionViewDataSource protocol
    //******************************************************************************

  
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let cellHeight = (collectionView.bounds.size.height - 10) / 3 // 3 count of rows to show
//        let cellWidth = (collectionView.bounds.size.width - 20) / 2 // 2 count of colomn to show
//        return CGSize(width: CGFloat(cellWidth), height: CGFloat(cellHeight))
//    }
    
    // tell the collection view how many cells to make
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return TotalReturn
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath as IndexPath) as! MyCollectionViewCell
        
        cell.bgView.layer.borderColor = UIColor.lightGray.cgColor
        cell.bgView.layer.borderWidth = 1
        cell.layer.masksToBounds = true
        // Use the outlet in our custom class to get a reference to the UILabel in the cell
        cell.titleLbl.text = self.items[indexPath.item]
        let imageName = self.items[indexPath.row]
        cell.inventoryImg.image = UIImage(named:imageName)
        
        return cell
    }
    
    
    //******************************************************************************
    // MARK: - UICollectionViewDelegate protocol
    //******************************************************************************
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        let secondViewController = self.storyboard?.instantiateViewController(withIdentifier: "vehicleDetailsVC") as! vehicleDetailsVC
        self.navigationController?.pushViewController(secondViewController, animated: true)
        
    }
    
    @IBAction func backBtn_clicked(_ sender: Any) {
        // Other View Controller
        self.navigationController?.popViewController(animated: true)
    }
}
